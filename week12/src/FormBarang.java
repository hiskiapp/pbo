import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;

public class FormBarang extends JDialog {
    private JPanel contentPane;
    private JButton btnStore;
    private JButton btnReset;
    private JTextField txtCode;
    private JTextField txtName;
    private JTextField txtStock;
    private JTextField txtStockMinimum;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JTable tblItems;
    private JLabel lblCode;
    private JLabel lblName;
    private JLabel lblUnit;
    private JLabel lblStock;
    private JLabel lblStockMinimum;
    private JLabel lblTitle;
    private JComboBox cbxUnit;

    public FormBarang() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnStore);

        showDatatable();
        generateCode();

        txtStock.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });

        txtStockMinimum.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
                    getToolkit().beep();
                    e.consume();
                }
            }
        });

        tblItems.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String code = tblItems.getValueAt(tblItems.getSelectedRow(),0).toString();
                find(code);
                btnStore.setEnabled(false);
            }
        });

        btnStore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (txtCode.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Kode barang tidak boleh kosong");
                    txtCode.requestFocus();
                }else if(txtName.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Nama barang tidak boleh kosong");
                    txtName.requestFocus();
                }else if(txtStock.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Stok barang tidak boleh kosong");
                    txtStock.requestFocus();
                }else if(txtStockMinimum.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Stok minimum barang tidak boleh kosong");
                    txtStockMinimum.requestFocus();
                }else{
                    String code = txtCode.getText();
                    String name = txtName.getText();
                    Object unit = cbxUnit.getSelectedItem();
                    int stock = Integer.parseInt(txtStock.getText());
                    int stock_minimum = Integer.parseInt(txtStockMinimum.getText());

                    if (isDuplicate(code)) {
                        JOptionPane.showMessageDialog(null, "Kode barang sudah ada");
                    } else {
                        store(code, name, unit, stock, stock_minimum);
                        showDatatable();
                        generateCode();
                    }
                }
            }
        });

        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (txtCode.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Kode barang tidak boleh kosong");
                    txtCode.requestFocus();
                }else if(txtName.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Nama barang tidak boleh kosong");
                    txtName.requestFocus();
                }else if(txtStock.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Stok barang tidak boleh kosong");
                    txtStock.requestFocus();
                }else if(txtStockMinimum.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Stok minimum barang tidak boleh kosong");
                    txtStockMinimum.requestFocus();
                }else {
                    String code = txtCode.getText();
                    String name = txtName.getText();
                    Object unit = cbxUnit.getSelectedItem();
                    int stock = Integer.parseInt(txtStock.getText());
                    int stock_minimum = Integer.parseInt(txtStockMinimum.getText());
                    if(isDuplicate(code)) {
                        update(code, name, unit, stock, stock_minimum);
                        JOptionPane.showMessageDialog(null, "Data telah diupdate");
                        showDatatable();
                        reset();
                    }else{
                        JOptionPane.showMessageDialog(null, "Kode barang tidak ditemukan");
                    }
                }
            }
        });

        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(tblItems.getSelectedRow() >= 0){
                    int response = JOptionPane.showConfirmDialog(null, "Yakin akan menghapus data data?");
                    if(response == 0)
                    {
                        String code = txtCode.getText();
                        destroy(code);
                        showDatatable();
                        reset();
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Batal hapus data!");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Pilih data terlebih dahulu!");
                }

            }
        });

        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
    }

    public void store(String code, String name, Object unit, int stock, int stock_minimum){
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO items (code,name,unit,stock,stock_minimum) VALUES (?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, code);
            ps.setString(2, name);
            ps.setObject(3, unit);
            ps.setInt(4, stock);
            ps.setInt(5, stock_minimum);

            ps.execute();

            stmt.close();
            conn.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void showDatatable(){
        DefaultTableModel model = new DefaultTableModel();

        model.addColumn("Kode Barang");
        model.addColumn("Nama Barang");
        model.addColumn("Satuan");
        model.addColumn("Stok Barang");
        model.addColumn("Stok Minimal");

        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM items");
            while(rs.next())
            {
                model.addRow(new Object[] {
                        rs.getString("code"),
                        rs.getString("name"),
                        rs.getString("unit"),
                        rs.getString("stock"),
                        rs.getString("stock_minimum"),
                });
            }

            stmt.close();
            conn.close();

            tblItems.setModel(model);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void update(String code, String name, Object unit, int stock, int stock_minimum){
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            PreparedStatement ps = conn.prepareStatement("UPDATE items SET name=?,unit=?,stock=?,stock_minimum=? WHERE code=?");
            ps.setString(1, name);
            ps.setObject(2, unit);
            ps.setInt(3, stock);
            ps.setInt(4, stock_minimum);
            ps.setString(5, code);

            ps.execute();

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void destroy(String code) {
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            PreparedStatement ps = conn.prepareStatement("DELETE FROM items WHERE code=?");
            ps.setString(1, code);

            ps.execute();

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void find(String code) {
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM items WHERE code=?");
            ps.setString(1, code);

            ResultSet rs = ps.executeQuery();

            rs.next();

            txtCode.setText(rs.getString("code"));
            txtName.setText(rs.getString("name"));
            cbxUnit.setSelectedItem(rs.getString("unit"));
            txtStock.setText(rs.getString("stock"));
            txtStockMinimum.setText(rs.getString("stock_minimum"));

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void reset() {
        txtCode.setText("");
        txtName.setText("");
        cbxUnit.setSelectedIndex(0);
        txtStock.setText("");
        txtStockMinimum.setText("");
        btnStore.setEnabled(true);
        generateCode();
    }

    public void generateCode(){
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT MAX(id) AS code FROM items");
            rs.next();

            int code = rs.getInt("code");
            code += 1;
            String new_code = "BRG" + String.format("%04d", code);
            txtCode.setText(new_code);

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean isDuplicate(String code){
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM items WHERE code=?");
            ps.setString(1, code);

            ResultSet rs = ps.executeQuery();
            boolean result = rs.next();

            stmt.close();
            conn.close();

            return result;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) {
        FormBarang dialog = new FormBarang();

        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
