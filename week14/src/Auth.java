import javax.swing.*;
import java.awt.event.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class Auth extends JDialog {
    private JPanel contentPane;
    private JButton btnLogin;
    private JButton buttonCancel;
    private JTextField txtUsername;
    private JLabel lblUsername;
    private JLabel lblPassword;
    private JPasswordField txtPassword;

    public Auth() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btnLogin);

        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onLogin();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onLogin() {
        // add your code here
        login();
    }

    private void login(){
        try {
            Connection conn = Conn.main();
            Statement stmt = conn.createStatement();

            String username = txtUsername.getText();
            String password = new String(txtPassword.getPassword());

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM users WHERE username=?");
            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                boolean checkMd5 = rs.getString("password").equals(getMd5(password));
                if (checkMd5) {
                    if(rs.getInt("is_active") == 1){
                        JOptionPane.showMessageDialog(null, "Login Success");
                    }else{
                        JOptionPane.showMessageDialog(null, "Your account is not active yet!");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Login Failed");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Account not found");
            }

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static String getMd5(String input)
    {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        Auth dialog = new Auth();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
