package uts;

public class CD extends AudioVideo {
    String artist;
    Integer numberOfTracks;

    CD(String title, String artist, Integer numberOfTracks) {
        super(title);
        this.artist = artist;
        this.numberOfTracks = numberOfTracks;
    }

    public void getArtist() {
        System.out.println("CD: " + title + " by " + artist);
    }
    
    public void getNumberOfTracks() {
        System.out.println("CD: " + title + " has " + numberOfTracks + " tracks");
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setNumberOfTracks(Integer numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }

    public void print() {
        super.print();
        getArtist();
        getNumberOfTracks();
    }


}
