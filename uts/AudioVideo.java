package uts;

import java.util.GregorianCalendar;

public class AudioVideo extends Item{
    int MAX_BORROW_DAYS = 7;

    public AudioVideo(String title) {
        super(title);
    }
    
    public void dueDate(GregorianCalendar borrowDate) {
        borrowDate.add(GregorianCalendar.DAY_OF_MONTH, MAX_BORROW_DAYS);
    }
}