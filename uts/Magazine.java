package uts;

public class Magazine extends Book {
    Integer volume;
    String type;

    Magazine(String title, String author, String publisher, Integer pages, Integer volume, String type) {
        super(title, author, publisher, pages);
        this.volume = volume;
        this.type = type;
    }

    public void getVolume() {
        System.out.println("Magazine: " + title + " volume " + volume);
    }

    public void getType() {
        System.out.println("Magazine: " + title + " type " + type);
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public void setType(String type) {
        this.type = type;
    }
}
