package uts;

public class Main {
    public static void main(String args[]){
        Journal journal = new Journal("The Journal", "John Doe", "John Doe", 100, 12345, "Journal");

        System.out.println("Title: " + journal.title);
        System.out.println("Author: " + journal.author);
        System.out.println("Publisher: " + journal.publisher);
        System.out.println("ISBN: " + journal.isbn);

    }
}
