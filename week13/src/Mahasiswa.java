public class Mahasiswa extends Manusia {
    public String nim;
    public double ipk;
    public boolean do_the_task;
    public boolean is_graduated;

    public Mahasiswa(String name, int age, int height, String nim, double ipk) {
        super(name, age, height);
        this.nim = nim;
        this.ipk = ipk;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public void setIpk(double ipk) {
        this.ipk = ipk;
    }

    public void setBiodata(String name, int age, int height, String nim, double ipk) {
        setName(name);
        setAge(age);
        setHeight(height);
        setNim(nim);
        setIpk(ipk);
    }

    public void setDoTheTask(boolean do_the_task) {
        this.do_the_task = do_the_task;
    }

    public void setIsGraduated(boolean is_graduated) {
        this.is_graduated = is_graduated;
    }

    public void getNim() {
        System.out.println("My nim is " + nim);
    }

    public void getIpk() {
        System.out.println("My ipk is " + ipk);
    }

    public void getBiodata() {
        getName();
        getAge();
        getHeight();
        getNim();
        getIpk();
    }

    public void setDoTheTask() {
        if (do_the_task) {
            System.out.println("I'm doing the task");
        } else {
            System.out.println("I'm not doing the task");
        }
    }

    public void setIsGraduated() {
        if (is_graduated) {
            System.out.println("I'm graduated");
        } else {
            System.out.println("I'm not graduated");
        }
    }
}
