public class Manusia {
    public String name;
    public int age;
    public int height;
    public boolean is_running;
    public boolean is_walking;
    public boolean is_eating;
    public boolean is_sleeping;

    public Manusia(String name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void  setAge(int age) {
        this.age = age;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setIsRunning(boolean is_running) {
        this.is_running = is_running;
    }

    public void setIsWalking(boolean is_walking) {
        this.is_walking = is_walking;
    }

    public void setIsEating(boolean is_eating) {
        this.is_eating = is_eating;
    }

    public void setIsSleeping(boolean is_sleeping) {
        this.is_sleeping = is_sleeping;
    }

    public void getName() {
        System.out.println("My name is " + name);
    }

    public void getAge() {
        System.out.println("I'm " + age + " years old");
    }

    public void getHeight() {
        System.out.println("I'm " + height + " cm tall");
    }


    public void setIsRunning() {
        if (is_running) {
            System.out.println("I'm running");
        } else {
            System.out.println("I'm not running");
        }
    }

    public void setIsWalking() {
        if (is_walking) {
            System.out.println("I'm walking");
        } else {
            System.out.println("I'm not walking");
        }
    }

    public void setIsEating() {
        if (is_eating) {
            System.out.println("I'm eating");
        } else {
            System.out.println("I'm not eating");
        }
    }

    public void setIsSleeping() {
        if (is_sleeping) {
            System.out.println("I'm sleeping");
        } else {
            System.out.println("I'm not sleeping");
        }
    }

}
