class Dog extends Animal {
    public void checkup() {
        System.out.println("Dog checkup");
    }

    @Override
    public void breath() {
        System.out.println("Dog breathing...");
    }
} 