class Bird extends Animal {
    public void eat() {
        System.out.println("Bird Eat");
    }

    @Override
    public void checkup() {
        System.out.println("Bird checkup");
    }

    @Override
    public void breath() {
        System.out.println("Bird breathing...");
    }
}