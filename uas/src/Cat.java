class Cat extends Animal implements LivingThing{
    public void checkup(){
        System.out.println("Cat checkup");
    }

    @Override
    public void breath() {
        System.out.println("Cat breathing...");
    }

    @Override
    public void walk() {
        System.out.println("Cat walking...");
    }

    @Override
    public void eat() {
        System.out.println("Cat eating...");
    }
}