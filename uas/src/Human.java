public class Human implements LivingThing
{
    public void breath(){
        System.out.println("Human breathing...");
    }

    @Override
    public void walk() {
        System.out.println("Human walking...");
    }

    @Override
    public void eat() {
        System.out.println("Human eating...");
    }
}